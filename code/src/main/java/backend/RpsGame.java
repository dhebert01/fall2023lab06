//David Hebert 2141781
package backend;
import java.util.Random;
public class RpsGame {
    private int win;
    private int loss;
    private int tie;
    private Random rand;

    public RpsGame(){
        this.win = 0;
        this.loss = 0;
        this.tie = 0;
        this.rand = new Random();
    }

    public int getWin(){
        return this.win;
    }

    public int getLoss(){
        return this.loss;
    }

    public int getTie(){
        return this.tie;
    }

    public String playRound(String playerChoice){
        String computerChoice ="";
        int randomNum = this.rand.nextInt(3);
        if(randomNum == 0){
            computerChoice = "rock";
        }
        else if(randomNum == 1){
            computerChoice = "scissors";
        }
        else if(randomNum == 2){
            computerChoice = "paper";
        }

        if(playerChoice.equals(computerChoice)){
            this.tie++;
            return ("Computer played " + computerChoice + " and you tied.");
        }
        else if(playerChoice.equals("rock") && computerChoice.equals("scissors")){
            this.win++;
            return ("Computer played " + computerChoice + " and you won.");
        }
        else if(playerChoice.equals("scissors") && computerChoice.equals("paper")){
            this.win++;
            return ("Computer played " + computerChoice + " and you won.");
        }
        else if(playerChoice.equals("paper") && computerChoice.equals("rock")){
            this.win++;
            return ("Computer played " + computerChoice + " and you won.");
        }
        else{
            this.loss++;
            return ("Computer played " + computerChoice + " and you loss.");
        }
    }
}
