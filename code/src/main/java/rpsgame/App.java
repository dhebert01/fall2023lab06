package rpsgame;
import java.util.Scanner;

import backend.RpsGame;
/**
 * Console application for rock paper scissors game
 *
 */
// David Hebert 2141781
public class App 
{
    public static void main( String[] args )
    {
        //TODO
        Boolean game = true;
        Scanner reader = new Scanner(System.in);
        RpsGame gameBoard = new RpsGame();
        while(game){
            System.out.println("\nPlease enter choice rock, paper, scissors or \"q\" to quit");
            String playerChoice = reader.nextLine();
            if(playerChoice.equals("q")){
                game = false;
                continue;
            }
            System.out.println(gameBoard.playRound(playerChoice));
        }
    }
}
